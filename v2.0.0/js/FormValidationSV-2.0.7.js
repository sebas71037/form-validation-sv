/**
* FV v2.0.7
* author: * naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
* Created Date: Tuesday October 11, 2016
* Updated Date: Monday October 17, 2016
* Description: A library to validation fields of form
* License: Apache 2.0
*/	
var $FV = function(){
	var self = this, _def = {
		'required':'Este campo es requerido',
		'email':'Correo no válido',
		'min':'Mínimo de dígitos permitidos: ',
		'max':'Máximo de dígitos permitidos: ',
		'number':'Digita solo números',
		'date':'Fecha no válida para el formato: ',
		'file':'No se permite archivos de tipo: ',
		'password-confirm':'Las contraseñas no coinciden.'
	}, _flags = {
		'max':false,
		'min':false,
		'date':false,
		'file':false
	}, _patterns = {'yyyy-mm-dd': /^\d{4}-\d{1,2}-\d{1,2}$/,
					'dd/mm/yyyy': /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/,
					'yyyy/mm/dd': /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/
					};

	this.init();

	var checkValue = function(a, b){
		for (var i = 0; i < a.length; i++) {
			if(a[i] === b){
				return true;
			}
		}
		return false;
	},getResponseValidate = function(r, def, msg){
		r.status = true;
	    r.msg = (msg === null || msg == '')? def : msg;
	}, initErrors = function(id){
		var _parent = document.getElementById(id);
		var _errors = _parent.getElementsByClassName("_error");
		for (var i = 0; i < _errors.length; i++) {
			_errors[i].style.display = 'none';
		}
	}, showErrors = function(obj){
		for (var i = 0; i < obj['validates'].length; i++) {
			var target = obj['validates'][i];
			target.obj_error.style.display = 'block';
			target.obj_error.innerHTML = target.msg;
		}
	}, valRequired = function(v){
		v = (typeof v == 'string' )? v.trim() : v;
		if(v === '' || v === undefined || v === false){
			return true;
		}
		return false;
	},validateCase = function(obj, required, v){
		v = v || obj.value;
		var r = {status:false, msg:''};
		var _r = valRequired(v);
		switch(required){
			case 'required':
				if(_r){
					getResponseValidate(r, _def['required'], obj.getAttribute('error-required'));
				}
				break;
			case 'email':
				var patron = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    		if(!patron.test(v) && !_r){
	    			getResponseValidate(r, _def['email'], obj.getAttribute('error-email'));
	    		}
				break;
			case 'min':
				var min = obj.getAttribute('data-min-s');
				if(min !== null && !isNaN(min) && !_r){
					if(v.toString().length < parseInt(min)){
						var msg = (_flags['min'])? _def['min']:_def['min']+min;
						getResponseValidate(r, msg, obj.getAttribute('error-min'));
					}
				}
				break;
			case 'max':
				var max = obj.getAttribute('data-max-s');
				if(max !== null && !isNaN(max) && !_r){
					if(v.toString().length > parseInt(max)){
						var msg = (_flags['max'])? _def['max']:_def['max']+max;
						getResponseValidate(r, msg, obj.getAttribute('error-max'));
					}
				}
				break;
			case 'number':
				var _r = valRequired(v);
				if(!_r){
					var pattern = /^[+-]?\d+(\.\d+)?$/.test(v);
					if(!pattern){
						getResponseValidate(r, _def['number'], obj.getAttribute('error-number'));	
					}
				}
				break;
			case 'date':
				var format = obj.getAttribute('data-dateformat-s');
				if(format !== null && isNaN(format) && !_r){
					var pattern = _patterns[format], flagD =  true;
					if(format === 'yyyy/mm/dd' && _patterns['yyyy-mm-dd'].test(v)){
						flagD = false;
					}
					if(pattern === undefined){
						throw '[Error 3]: El formato de fecha no es válido.';
					}else if(!pattern.test(v) || !flagD){
						var msg = (_flags['date'])? _def['date']:_def['date']+format;
						getResponseValidate(r, msg, obj.getAttribute('error-date'));		
					}
				}
				break;
			case 'file':
				var types = obj.getAttribute('data-typesfile-s');
				if(obj.type === 'file' && types !== null && types != '' && !_r){
					types = types.split(',');
					var target = v.name.split('.');
					target = target[target.length-1];
					if(!checkValue(types, target)){
						var msg = (_flags['file'])? _def['file']:_def['file']+target;
						getResponseValidate(r, msg, obj.getAttribute('error-file'));
					}
				}
				break;
			case 'password-confirm':
				if(!_r){
					var target_name = obj.getAttribute('data-password-confirm');
					var target = obj.form.querySelectorAll('[name="'+target_name+'"]')[0];
					if(v !== target.value){
						getResponseValidate(r, _def['password-confirm'], obj.getAttribute('error-password-confirm'));
					}
				}
				break;
		}
		return r;
	}, validate = function(obj, v){
		v = v || obj.value;
		var r = {status: false, msg: ''};
		var _requireds = obj.getAttribute('data-required-values');
		_requireds = (_requireds === null || _requireds == '')? [] : _requireds.split(',');
		for (var i = 0; i < _requireds.length; i++) {
			r = validateCase(obj, _requireds[i], v);
			if(r.status === true){
				return r;
			}
		}
		return r;
	}, validateField = function(obj, b, n, v){
		n = n || obj.name;
		v = v || obj.value;
		var r = validate(obj, v);
		if(b !== false){
			b.append(n, v);	
		}
		return r;
	}, validateFiles = function(obj, b){
		var r = {};
		if(obj.files.length > 0){
				self.prepareFiles(obj, b, function(i, e){
					r =	validateField(obj, b, obj.name+'['+i+']', e);
					if(r.status){
						return;
					}
				});
		}else{
			r = validateField(obj, b, '');
		}
		return r;
	}, validatesRequireds = function(a, _v, obj, id){
		if(a.status === true){
			var element = document.getElementById(id).querySelectorAll('[data-target-s="'+obj.name+'"]')[0];
			if(obj.name == ""){
				throw '[Error 1]: El atributo name no puede estar vacío.';
			}else if(element === undefined){
				throw '[Error 2]: El elemento _error del campo name="'+obj.name+'" no fue encontrado.';
			}else{
				_v['status'] = true;
				_v['validates'].push({
					msg: a.msg,
					obj: obj,
					obj_error: element
				});	
			}
		}
	}, findI = function(a, b){
		for (var k in a) {
			if(k === b){
				return true;
			}
		}
		return false;
	}, vInputs = function(a, id, _view, b){
		_view =  _view || false;
		b = b  || false;
		var _validates = {status:false, validates:[]};
		for (var i = 0; i < a.length; i++) {
			switch(a[i].type){
				case 'file':
					validatesRequireds(validateFiles(a[i], b), _validates, a[i], id);
					break;
				case 'checkbox':
					var val = (a[i].checked === false)? ' ':true;
					validatesRequireds(validateField(a[i], b, false, val), _validates, a[i], id);
					break;
				case 'radio':
					var radios = document.getElementById(id).querySelectorAll('[name="'+a[i].name+'"]:checked');
					if(a[i].checked === true){
						validatesRequireds(validateField(a[i], b), _validates, a[i], id);	
					}else if(radios.length === 0){
						validatesRequireds(validateField(a[i], b, a[i].name, ' '), _validates, a[i], id);	
					}
					break;
				default:
					validatesRequireds(validateField(a[i], b), _validates, a[i], id);
					break;
			}
		}

		if(_view){
			showErrors(_validates);
		}
		return _validates;
	};

	this.addEvent = function(id, obj, event, _view, callback){
		obj = (obj instanceof HTMLElement)? [obj]:obj;
		for (var i = 0; i < obj.length; i++) {
			obj[i].addEventListener(event, function(e){
				var v = vInputs([this], id);
				if(v.status && _view){
					self.showError(v['validates'][0]);
				}else if(_view){
					document.getElementById(id).querySelectorAll('[data-target-s="'+this.name+'"]')[0].style.display = 'none';
				}
				if(callback){
					callback(v);
				}
			});
		}
	}

	this.showError = function(target){
		target.obj_error.style.display = 'block';
		target.obj_error.innerHTML = target.msg;
	};

	this.setDefaultMsg = function(k, v){	
		_def[k] = v;
		if(findI(k)){
			_flags[k] = true;
		}
	};

	this.validateInputs = function(a, id, _view, b){
		initErrors(id);
		return vInputs(a, id, _view, b);
	};

}

$FV.prototype = {
	init: function(){
		for (var k in $FV) {
			this[k] = $FV[k];
		}
	}
}

$FV.prepareFiles = function(obj, b, callback){
	for (var i = 0; i < obj.files.length; i++) {
		if(callback){
			callback(i, obj.files[i]);	
		}else{
			b.append(obj.name+'['+i+']', obj.files[i]);
		}
	}
}

$FV.prepareForm = function(a, b){
	for (var i = 0; i < a.length; i++) {
		switch(a[i].type){
			case 'file':
				$FV.prepareFiles(a[i], b);
				break;
			default:
				b.append(a[i].name, a[i].value);
				break;
		}
	}
}

$FV.viewEntries = function(a){
	return Array.from(a.entries());
}